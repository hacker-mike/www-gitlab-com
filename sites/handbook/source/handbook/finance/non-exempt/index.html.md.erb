---
layout: handbook-page-toc
title: "Non-Exempt Policies & Processes"
description: "Clarifying the GitLab Policies and Processes related to US-based Non-Exempt Employees."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

For GitLab's US-based team members employed through GitLab Inc or GitLab Federal LLC, the United States [Fair Labor Standards Act](https://www.dol.gov/agencies/whd/flsa), provides guidelines on employment status, child labor, minimum wage, overtime pay, and record-keeping requirements. It determines which employees are exempt from the Act (not covered by it) and which are non-exempt (covered by the Act). It establishes wage and time requirements when minors can work, sets the minimum wage that must be paid, and mandates when overtime must be paid. Please review the [FLSA Digital Reference Guide](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/Digital_Reference_Guide_FLSA.pdf) for additional information.

## Definitions

**Exempt**: An individual who is exempt from the overtime provisions of the Fair Labor Standards Act (FLSA) because they are classified as an executive, professional, administrative or outside sales employee, and meet the specific criteria for the exemption. Certain computer professionals may also be exempt. With some limited exceptions, exempt employees must be paid on a salary basis. Exempt employees do not need to track their hours, and are not eligible for overtime pay. These employees must not spend more than 50% of their time doing non-exempt duties to remain exempt.

**Non-exempt**: An individual who is not exempt from the overtime provisions of the FLSA and is therefore entitled to overtime pay for all hours worked beyond 40 in a workweek (as well as any additional state overtime provisions). Non-exempt employees may be paid on a salary, hourly or other basis. They are required to track and report all hours worked. If a non-exempt employee works more than 40 hours during a workweek, they are entitled to overtime pay. For more information, review the [overtime](/handbook/finance/non-exempt/#overtime) section below. Working overtime hours must be approved prior to the hours worked by the manager.  

**Salaried**: An individual who receives the same salary from week to week regardless of how many hours he or she works. Exempt employees must be paid on a salary basis, as discussed above. Non-exempt employees may be paid on a salary basis for a fixed number of hours or under the fluctuating workweek method. Salaried non-exempt employees must still receive overtime in accordance with federal and state laws.

**Hourly**: An individual who receives an hourly wage for work performed. Generally, such individuals, because of the method of payment, are classified as non-exempt and are subject to the overtime provisions of the FLSA. Exempt computer professionals may also be paid on an hourly basis and still retain their exempt status.

## Meal and Rest Breaks

Breaks are an important part of your day. At GitLab, your well-being is critical. For your mental and physical health, please take at least the minimum listed breaks as listed below and **always** log your unpaid meal breaks. This is to ensure that we adhere to the United States labor laws.  It is GitLab’s policy to comply with **all** laws regarding meal and rest breaks. 

As a fully distributed company, any team member classified as a non-exempt employee is responsible for taking active ownership in ensuring they take the proper breaks. Please contact the Team Member Relations team at `teammemberrelations@gitlab.com` if you feel that your circumstances are preventing you from doing so.

### Meal Break Policy

* Team members **must** clock in and out for meal periods in their Timekeeping portal in ADP.
* Team members who work between five and six hours a day are entitled to an unpaid meal break of at least 30 minutes.
* Team members who work more than six hours a day are entitled to an unpaid meal break of at least 45 minutes.
* The meal period must begin no later than the end of a team member's fifth hour of work.
* A second unpaid meal break of at least 30 minutes is due after 10 hours total worked in a single day.
* Team members should be completely relieved of duty during their meal break.
* To the extent that the local law of the jurisdiction where the team member resides entitles a team member to longer breaks or breaks more often than as is set out in this policy, the local law applies.

### Rest Break Policy

* Team members **do not** log their rest breaks in their Timekeeping portal in ADP.
* Team members who work for at least two hours are entitled to one paid rest break of at least 10 minutes, and additional paid 10-minute rest breaks for every three and a half hours worked thereafter.
* Team members may not work more than three continuous hours without a rest break.

## Overtime

When operating requirements or other needs cannot be met during regular working hours, team members may be scheduled to work overtime. Paid time off such as sick pay, holiday pay, vacation pay and jury duty pay (where applicable) will not count toward hours worked for the purpose of determining overtime pay.

Non-exempt team members must report all time worked and not work any time that is not authorized by their managers. This means non-exempt team members must not start work early, finish work late, work during a meal or rest break, or perform any other extra or overtime work unless directed to do so. Team members who have questions about when or how many hours they are expected to work should contact their manager. If they have any additional concerns, they should reach out to Team Member Relations at `teammemberrelations@gitlab.com`.  

It is a violation of GitLab’s policy for anyone to instruct or encourage another team member to work “off the clock,” to incorrectly report hours worked, or to alter another team member’s time records. If any team member is directed or encouraged to incorrectly report hours worked, or to alter another team member’s time records, they should report the incident immediately to Team Member Relations at `teammemberrelations@gitlab.com`. 

GitLab requires manager approval prior to non-exempt team members working overtime. Unless a non-exempt team member resides in one of the below states, overtime is working over 40 hours in a workweek, and results in a pay of 1.5x. A workweek is a Sunday to Saturday week. 


| State  | Overtime Requirements        |
|---------------|-----------------------------------------------|
| Alaska        | 1.5x for over 8 hrs in any workday.           |
| California    | 1.5x for over 8 hrs up in any workday, and for the first 8 hrs on the 7th consecutive day of work in a workweek; 2x for over 12 hrs in any workday and over 8 hrs on the 7th consecutive day of a workweek |
| Colorado      | 1.5x for over 12 hrs in any workday, or over 40 hrs in a workweek, whichever equals greater wages.       |
| Rhode Island  | 1.5x for any hours worked on a Sunday or on certain holidays.   |

## Overtime Approvals

All overtime work must be authorized in advance by the team member’s manager. The process to request overtime is done in a manner agreed upon between manager and team member; by email only, for documentation purposes.

Working overtime without prior authorization may result in disciplinary action. It is the manager's responsibility to review timecards. If overtime is submitted without prior authorization, please communicate with your direct report to better understand what happened, and to reiterate our overtime approvals policy.

## ADP Timekeeping Portal

